package br.com.caelum.livraria.bean;

import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.caelum.livraria.dao.AutorDAO;
import br.com.caelum.livraria.dao.LivroDAO;
import br.com.caelum.livraria.modelo.Autor;
import br.com.caelum.livraria.modelo.Livro;
import br.com.caelum.livraria.modelo.LivroDataModel;
import br.com.caelum.livraria.tx.Transactional;
import br.com.caelum.livraria.util.RedirectView;

@Named
@ViewScoped
public class LivroBean implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Inject
	private Livro livro;
	
	private Integer autorId;
	private Integer livroId;
	private List<Livro> livros;
	
	@Inject
	private LivroDataModel dataModel;
	
	@Inject
	private LivroDAO livroDAO;
	
	@Inject
	private AutorDAO autorDAO;

	@Inject
	private FacesContext context;
	

	public Integer getLivroId() {
		return livroId;
	}

	public void setLivroId(Integer livroId) {
		this.livroId = livroId;
	}

	public void setLivro(Livro livro) {
		this.livro = livro;
	}

	public List<Autor> getAutoresDoLivro() {
		return this.livro.getAutores();
	}

	public Integer getAutorId() {
		return autorId;
	}

	public void setAutorId(Integer autorId) {
		this.autorId = autorId;
	}

	public Livro getLivro() {
		return livro;
	}

	/** lista os livros e verifica se os livros ja foram carregados na requisi�ao
	 * se nao inicia os livros
	 * 
	 * @return retorna a lista de livros
	 */
	public List<Livro> getLivros() {

		if (this.livros == null) {
			this.livros = livroDAO.listaTodos();
		}
		return livros;
	}

	public List<Autor> getAutores() {
		
		return autorDAO.listaTodos();
	}

	public void carregar(Livro livro) {

		this.livro = livro;
	}

	public void carregaLivroPelaId() {

		this.livro = livroDAO.buscaPorId(livroId);
	}

	@Transactional
	public RedirectView remover(Livro livro) {

		livroDAO.remove(livro);
		context.getExternalContext().getFlash().setKeepMessages(true);
		context.addMessage(null, new FacesMessage("livro removido com sucesso"));
		return new RedirectView("livro");
	}

	@Transactional
	public void gravarAutor() {
		Autor autor = autorDAO.buscaPorId(autorId);
		System.out.println("Gravando autor ... " + autor.getNome());
		this.livro.adicionaAutor(autor);

	}

	@Transactional
	public RedirectView gravar() {
		System.out.println("Gravando livro " + this.livro.getTitulo());

		if (livro.getAutores().isEmpty()) {
			// throw new RuntimeException("Livro deve ter pelo menos um Autor.");
			context.addMessage("autor",
					new FacesMessage("Livro deve ter pelo menos um Autor."));
		} else {

			if (livro.getId() == null) {
				
				 livroDAO.adiciona(this.livro);
				 this.livros = livroDAO.listaTodos();
				 

			} else {
				livroDAO.atualiza(this.livro);
			}

		}

		// usa o contexto flash para exibir a mensagem apos o redirect
		context.getExternalContext().getFlash().setKeepMessages(true);
		context.addMessage(null, new FacesMessage("livro cadastrado com sucesso"));
		return new RedirectView("livro");

	}

	public void comecaComDigitoUm(FacesContext fc, UIComponent ui, Object value) throws ValidatorException {
		String valor = value.toString();
		if (!valor.startsWith("1")) {
			throw new ValidatorException(new FacesMessage("ISBN deveria come�ar com digito 1"));
		}
	}

	public String formAutor() {

		return "autor?faces-redirect=true";
	}

	public LivroDataModel getDataModel() {
		return dataModel;
	}

	public void setDataModel(LivroDataModel dataModel) {
		this.dataModel = dataModel;
	}
}

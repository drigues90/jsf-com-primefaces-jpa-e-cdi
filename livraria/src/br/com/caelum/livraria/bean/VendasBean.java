package br.com.caelum.livraria.bean;

import java.io.Serializable;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.ChartSeries;

import br.com.caelum.livraria.dao.VendaDAO;
import br.com.caelum.livraria.modelo.Venda;

@Named
@ViewScoped
public class VendasBean implements Serializable{

	private static final long serialVersionUID = 1L;

	@Inject
	private VendaDAO vendaDAO;

	private List<Venda> vendas;

	public BarChartModel getVendasModel() {

	    BarChartModel model = new BarChartModel(); 
	    
	    model.setAnimate(true);

	    ChartSeries vendaSerie = new ChartSeries();
	    vendaSerie.setLabel("Vendas 2016");

	    vendas = getVendas();

	    for (Venda venda : vendas) {
	        vendaSerie.set(venda.getLivro().getTitulo(), venda.getQuantidade());
	    }

	    model.addSeries(vendaSerie);
	    
	    model.setTitle("Vendas"); // setando o t�tulo do gr�fico
	    model.setLegendPosition("ne"); // nordeste

	    // pegando o eixo X do gr�fico e setando o t�tulo do mesmo
	    Axis xAxis = model.getAxis(AxisType.X);
	    xAxis.setLabel("T�tulo");

	    // pegando o eixo Y do gr�fico e setando o t�tulo do mesmo
	    Axis yAxis = model.getAxis(AxisType.Y);
	    yAxis.setLabel("Quantidade");

	    return model;
	}

	public List<Venda> getVendas() {

		this.vendas = vendaDAO.getVenda();

	    return vendas;
	}
}
package br.com.caelum.livraria.bean;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.caelum.livraria.dao.UsuarioDAO;
import br.com.caelum.livraria.modelo.Usuario;
import br.com.caelum.livraria.util.RedirectView;

@Named
@javax.faces.view.ViewScoped
public class LoginBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private Usuario usuario = new Usuario();
	
	@Inject
	private FacesContext context;

	@Inject
	private UsuarioDAO usuarioDAO;

	public RedirectView efetuaLogin() {

		System.out.println("Fazendo login do usuairo " + this.usuario.getEmail());

		System.out.println("aide");
		boolean existe = usuarioDAO.existe(this.usuario);	
		
		if (existe) {

			context.getExternalContext().getSessionMap().put("usuarioLogado", this.usuario);
			
			return new RedirectView("livro");
		}
		
		context.getExternalContext().getFlash().setKeepMessages(true);
		context.addMessage(null, new FacesMessage("usuario nao encontrado"));
		return new RedirectView("login");
	}
	
	public RedirectView deslogar() {
		
		context.getExternalContext().getSessionMap().remove("usuarioLogado");
		
		context.getExternalContext().getFlash().setKeepMessages(true);
		context.addMessage(null, new FacesMessage("usuario deslogado com sucesso"));
		
		return new RedirectView("login");
		
	}

	public Usuario getUsuario() {
		return usuario;
	}

}

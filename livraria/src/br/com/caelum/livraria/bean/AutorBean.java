package br.com.caelum.livraria.bean;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.caelum.livraria.dao.AutorDAO;
import br.com.caelum.livraria.modelo.Autor;
import br.com.caelum.livraria.tx.Transactional;

@Named
@ViewScoped
public class AutorBean implements Serializable{

	private static final long serialVersionUID = 1L;

	private Autor autor = new Autor();
	
	private Integer autorId;
	
	@Inject
	private FacesContext context;

	@Inject
	private AutorDAO dao;

	public Integer getAutorId() {
		return autorId;
	}

	public void setAutorId(Integer autorId) {
		this.autorId = autorId;
	}
	
	public void setAutor(Autor autor) {
		this.autor = autor;
	}

	public Autor getAutor() {
		return autor;
	}
	
	public void carregaAutorPelaId() {
		
		this.dao.buscaPorId(autorId);
	}

	@Transactional
	public String gravar() {
		System.out.println("Gravando autor " + this.autor.getNome());

		this.dao.adiciona(this.autor);
		
		context.addMessage(null,
				new FacesMessage("Autor cadastrado com sucesso"));
		
		return "livro?faces-redirect=true";
		
	}
	
	@Transactional
	public void remover(Autor autor) {

		this.dao.remove(autor);
		context.addMessage(null, new FacesMessage("Autor excluido com sucesso"));
	}
}

package br.com.caelum.livraria.modelo;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.primefaces.model.LazyDataModel;

import br.com.caelum.livraria.dao.LivroDAO;

public class LivroDataModel extends LazyDataModel<Livro> implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private LivroDAO livroDAO;
	
	
	@PostConstruct
	void init() {
		
		livroDAO.quantidadeDeElementos();
	}
	
	/*@Override
	public List<Livro> load(int inicio, int quantidade, String campoOrdenacao, SortOrder sentidoOrdenacao, Map<String, Object> filtros) {
	    String titulo = (String) filtros.get("titulo");

	    return dao.listaTodosPaginada(inicio, quantidade, "titulo", titulo);
	}*/
}

package br.com.caelum.livraria.dao;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaQuery;

public class DAO<T> implements Serializable {

	private static final long serialVersionUID = 1L;

	private final Class<T> classe;

	@Inject
	private EntityManager em;

	public DAO(EntityManager manager, Class<T> classe) {
		this.em = manager;
		this.classe = classe;
	}

	public void adiciona(T t) {

		em.persist(t);

	}

	public void remove(T t) {

		em.remove(em.merge(t));

	}

	public void atualiza(T t) {

		em.merge(t);

	}

	public List<T> listaTodos() {
		CriteriaQuery<T> query = em.getCriteriaBuilder().createQuery(classe);
		query.select(query.from(classe));

		List<T> lista = em.createQuery(query).getResultList();

		return lista;
	}

	public T buscaPorId(Integer id) {
		T instancia = em.find(classe, id);
		return instancia;
	}

	public int contaTodos() {
		long result = (Long) em.createQuery("select count(n) from livro n").getSingleResult();

		return (int) result;
	}

	/**
	 * metodo visa adequar o carregamento paginado junto com o filtro de busca porem
	 * devido a erro "desconhecido na linha 90, nao foi possivel implementalo.
	 * 
	 */
	/*
	 * public List<T> listaTodosPaginada(int firstResult, int maxResults,String
	 * coluna, String valor) { EntityManager em = new JPAUtil().getEntityManager();
	 * CriteriaQuery<T> query = em.getCriteriaBuilder().createQuery(classe);
	 * query.select(query.from(classe));
	 * 
	 * if (valor != null) query =
	 * query.where(em.getCriteriaBuilder().like(root.<String>get(coluna), valor +
	 * "%"));
	 * 
	 * List<T> lista =
	 * em.createQuery(query).setFirstResult(firstResult).setMaxResults(maxResults).
	 * getResultList();
	 * 
	 * em.close(); return lista; }
	 */

	/**
	 * metodo suporte para buscar os dados conforme ocorre a paginašao na tabela de
	 * livros
	 * 
	 * @return retorna a quantidade de livros
	 */
	public int quantidadeDeElementos() {
		long result = (Long) em.createQuery("select count(n) from " + classe.getSimpleName() + " n").getSingleResult();

		return (int) result;
	}

}

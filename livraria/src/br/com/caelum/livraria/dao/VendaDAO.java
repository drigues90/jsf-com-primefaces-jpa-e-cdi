package br.com.caelum.livraria.dao;

import java.io.Serializable;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;

import br.com.caelum.livraria.modelo.Venda;

@Named
@ViewScoped
public class VendaDAO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private EntityManager manager;

	public List<Venda> getVenda() {
		
		return manager.createQuery("select v from Venda v",Venda.class).getResultList();
	}
	
}

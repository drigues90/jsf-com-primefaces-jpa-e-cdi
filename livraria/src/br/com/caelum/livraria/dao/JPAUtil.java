package br.com.caelum.livraria.dao;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JPAUtil {

	private static EntityManagerFactory emf = Persistence
			.createEntityManagerFactory("livraria");

	@Produces // cria o entity manager
	@RequestScoped // cria um entity manage por requeisição
	public EntityManager getEntityManager() {
		return emf.createEntityManager();
	}

	// fecha o entity manager apos o fim da requisição
	public void close(@Disposes EntityManager em) {
		em.close();
	}
}

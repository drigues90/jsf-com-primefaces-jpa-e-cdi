package br.com.caelum.livraria.dao;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import br.com.caelum.livraria.modelo.Autor;
import br.com.caelum.livraria.util.Log;

public class AutorDAO implements Serializable{
	
	private static final long serialVersionUID = 1L;

	private DAO<Autor>dao;
	
	@Inject
	EntityManager em;
	
	@Log // atividade do curso
	@PostConstruct
	void init() {
		this.dao = new DAO<Autor>(this.em,Autor.class);
	}
	
	public void adiciona(Autor t) {
		dao.adiciona(t);
	}

	public void remove(Autor t) {
		dao.remove(t);
	}

	public void atualiza(Autor t) {
		dao.atualiza(t);
	}

	public List<Autor> listaTodos() {
		return dao.listaTodos();
	}

	public Autor buscaPorId(Integer id) {
		return dao.buscaPorId(id);
	}

	public int quantidadeDeElementos() {
		return dao.quantidadeDeElementos();
	}
	
}

package br.com.caelum.livraria.tx;

import java.io.Serializable;

import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import javax.persistence.EntityManager;

import org.jboss.logging.Logger;

/**
 * gerencia as transa��es atraves do cdi, evitando que os metodos de persitencia
 * abram e fecham trasa��es constemente.
 * @author jonathan.jesus
 *
 */
@Transactional
@Interceptor // avisa que esta classe intercepta as anota��es @transacional
public class GerenciadorDeTransacao implements Serializable{
	
	@Inject
	private EntityManager manager;
	
	private static Logger log = Logger.getLogger(GerenciadorDeTransacao.class.getName());

	private static final long serialVersionUID = 1L;

	@AroundInvoke //avisa o cdi que este metodo deve ser executado 
	public Object executaTX(InvocationContext context) throws Exception {
		
		log.info("inciando transa��o");
		manager.getTransaction().begin();
		
		// executa o que esta entre o incido da transa�ao e o fim
		Object result = context.proceed();
		
		manager.getTransaction().commit();
		log.info("transa�ao finalizada");
		
		return result;
	}
}

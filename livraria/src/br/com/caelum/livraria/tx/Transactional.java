package br.com.caelum.livraria.tx;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.interceptor.InterceptorBinding;

@Retention(RetentionPolicy.RUNTIME) // seta anota��o para execu��o
@Target({ElementType.METHOD, ElementType.TYPE}) // diz que pode ser usada no escopo classe e metodo.
@InterceptorBinding // avisa que esta anota��o esta ligada com um interceptador
public @interface Transactional {

}

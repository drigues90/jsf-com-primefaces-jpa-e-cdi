package br.com.caelum.livraria.util;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

/**
 * esta classe foi criado como atividade do curso
 * usada para gerneciar o log que monitora o tempo incial e final das requisi�oe
 * anotadas com @Log
 * @author jonathan.jesus
 *
 */
@Log
@Interceptor
public class TempoDeExecucaoInterceptor implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private static Logger logger = Logger.getLogger(TempoDeExecucaoInterceptor.class.getName());

	@AroundInvoke
	public Object loggin(InvocationContext context) throws Exception {
		
		long antes = System.currentTimeMillis();
		logger.log(Level.INFO, "incido da requsi��o " + antes);
		
		Object result = context.proceed();
		
		long depois = System.currentTimeMillis();
		logger.log(Level.INFO, "fim da requsi��o " + depois);
		
		return result;
		
	}
}
